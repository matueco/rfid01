#include "rrtc.h"

void rtcConfig( rtc_t * rtc ){


      Chip_RTC_Init(LPC_RTC);
      
      rtcWrite( rtc );

      Chip_RTC_Enable(LPC_RTC, ENABLE);
}


void rtcRead( rtc_t * rtc ){

   
   RTC_TIME_T rtcTime;

   Chip_RTC_GetFullTime(LPC_RTC, &rtcTime);

   rtc->sec = rtcTime.time[RTC_TIMETYPE_SECOND];
   rtc->min = rtcTime.time[RTC_TIMETYPE_MINUTE];
   rtc->hour = rtcTime.time[RTC_TIMETYPE_HOUR];
   rtc->wday = rtcTime.time[RTC_TIMETYPE_DAYOFWEEK];
   rtc->mday = rtcTime.time[RTC_TIMETYPE_DAYOFMONTH];
   rtc->month = rtcTime.time[RTC_TIMETYPE_MONTH];
   rtc->year = rtcTime.time[RTC_TIMETYPE_YEAR];

}

void rtcWrite( rtc_t * rtc ){

   RTC_TIME_T rtcTime;

   rtcTime.time[RTC_TIMETYPE_SECOND]     = rtc->sec;
   rtcTime.time[RTC_TIMETYPE_MINUTE]     = rtc->min;
   rtcTime.time[RTC_TIMETYPE_HOUR]       = rtc->hour;
   rtcTime.time[RTC_TIMETYPE_DAYOFMONTH] = rtc->wday;
   rtcTime.time[RTC_TIMETYPE_DAYOFMONTH] = rtc->mday;
   rtcTime.time[RTC_TIMETYPE_MONTH]      = rtc->month;
   rtcTime.time[RTC_TIMETYPE_YEAR]	     = rtc->year;

   Chip_RTC_SetFullTime(LPC_RTC, &rtcTime);

   
}

