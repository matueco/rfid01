#ifndef _SAPI_RTC_H_
#define _SAPI_RTC_H_


#include "board.h"
#include "rtc_18xx_43xx.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
   uint16_t year;	 /* 1 to 4095 */
   uint8_t  month;	 /* 1 to 12   */
   uint8_t  mday;	 /* 1 to 31   */
   uint8_t  wday;	 /* 1 to 7    */
   uint8_t  hour;	 /* 0 to 23   */
   uint8_t  min;	 /* 0 to 59   */
   uint8_t  sec;	 /* 0 to 59   */
} rtc_t;

void rtcConfig( rtc_t * rtc );
void rtcRead( rtc_t * rtc );
void rtcWrite( rtc_t * rtc );


#ifdef __cplusplus
}
#endif

/*==================[end of file]============================================*/
#endif /* _SAPI_RTC_H_ */
